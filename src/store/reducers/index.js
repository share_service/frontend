import { userInfoReducer } from './userReducer';
import { goalsReducer } from './goalsReducer';

export default {
	userInfo: userInfoReducer,
	goals: goalsReducer,
};
