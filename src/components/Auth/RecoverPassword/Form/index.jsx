import React, {useState} from 'react';
import useInput from './../../ValidationHook/index';
import Email from '../../../Email';
import { recoverUser } from '../../../../store/actions';
import { useDispatch } from "react-redux";


//Recover Password

function Form()  {
	const email = useInput('', {isEmpty: true, minLength: 5, isEmail: true});
	const dispatch = useDispatch();
	const [error, setError] = useState('');

	const handleError = () => {
	
		setError('Incorrect email');
	
	};
	
	const handleRecover = () => {
		
		console.log('Кнопка работает');
		const newUserData = {
			email: email.value,
		};	
		dispatch(recoverUser(newUserData, handleError));
		
	};

	return (
		
		<form 
			className="front__form"
			onSubmit={event => {
				event.preventDefault();
				event.target.reset();	
				
			}}
		>

			
			{/* Email  */}
			<div className = 'error-message'>{ error }</div>


			{(email.isDirty && email.isEmpty) && 
				<div className="validation-text">empty field</div>}
			{(email.isDirty && email.emailError) && 
				<div className="validation-text">enter valid email</div>}
			
			{(email.isDirty && email.isEmpty) || (email.isDirty && email.emailError) ?
			// true					 true				true
				<Email 
					value={email.value}
					onChange={e => email.onChange(e)}
					onBlur={e => email.onBlur(e)}
					style = {{border: '1px solid #EB5757'}}
				/> 
				: <Email 
					value={email.value}
					onChange={e => email.onChange(e)}
					onBlur={e => email.onBlur(e)}
				/>
					
			}
			
			{/* Кнопка */}
			<button
				type = "submit" 
				className = "button"
				disabled = {!email.inputValid}
				onClick = { handleRecover }
			>
						send me email instructions
			</button>
	
		</form>
		
	);
	
}


export default Form;